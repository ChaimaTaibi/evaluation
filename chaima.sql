#-- 1) Afficher les patients par ordre croissant de date de naissance.

select * 
from patient 
order by date_naissance;

#-- 2) Combien y a t'il de patients femme (sexe = 2) ?

select count(id_patient)
from patient
where sexe == 2;

select *
from patient 

#-- 3) Combien de patients sont nés avant le 01/01/1930 ?

select count(date_naissance)
from patient 
where date_naissance > 01/01/1930;

#-- 4) Combien de séjour durent plus de 10 jours (> 10) ?

select count(id_sejour)
from sejour 
where date_sortie > 10; 

#-- 5) Afficher les idenfiants des patients qui n'ont qu'un seul séjour ?

select id_patient
from sejour
where id_sejour = 1;

#-- 6) Combien y a-t'il de RUMs dans l'UF ME15 ?

select count(id_rum) 
from rum 
where code_uf = 'ME15';

#-- 7) Affichier les codes UF dans lesquelles est passé le patient 10 ?

select distinct(code_uf) 
from rum 
where id_sejour = 10;

#-- 8) Afficher tous les RUMs qui ne se sont pas déroulés dans l'UF ME12.

select (id_sejour)
from rum 
where <> 'ME12';

#-- 9) Afficher le nombre de RUMs par code UF et par nombre décroissant.

select id_rum
from rum 
order by code_uf desc 

#-- 10) Afficher l'âge à l'admission du séjour




